


# KONSTITUSI VIGOR DAC


## 1. INTI PRINSIP

1.1. Komunitas otonom terdesentralisasi ini (“DAC”) akan dikenal sebagai VIGOR DAC dan diatur oleh Konstitusi ini, dikelola dan ditegakkan melalui media teknologi blockchain. Hash teks sumber Konstitusi ini direkam secara permanen di blockchain dengan versi yang dicatat dan hanya dapat diubah dengan metode dan persyaratan yang dinyatakan di sini.

1.2. VIGOR DAC didirikan pada prinsip, misi, nilai, dan tujuan inti berikut ini:

a) Membangun, mengkode, dan mengelola perangkat lunak sumber terbuka yang berpusat di sekitar gagasan yang diungkapkan dalam Whitepaper VIGOR dan paten apa pun yang akan datang: VIGOR DAC akan selalu berusaha untuk memastikan bahwa ia mampu membangun perangkat lunak sumber terbuka untuk blockchain yang digerakkan oleh perangkat lunak EOSIO, atau rantai lain seperti yang ditentukan. VIGOR DAC harus memfokuskan upayanya pada pembangunan, pemeliharaan, pengoperasian, dan penyimpanan, kode untuk protokol VIGOR dan semua Kontrak Cerdas dan platform perangkat lunak yang terkait dengan Whitepaper VIGOR serta paten dan ekstensi yang akan datang yang disetujui oleh DAC. Tujuan utama yang mendasarinya harus selalu menciptakan dan memelihara solusi Desentralized Finance (DeFi).

b) Keterbukaan dan Transparansi: Semua keputusan yang dibuat oleh struktur tata kelola VIGOR DAC dan semua operasinya akan terbuka dan transparan dan dicatat secara permanen di blockchain. Tidak ada aktor tunggal dan/atau individu yang memiliki kekuatan untuk menyensor protokol.

c) Keadilan: VIGOR DAC akan memperlakukan semua anggota secara adil, menghargai kontribusi dengan tepat dan menjaga kebijakan nirlaba. Tidak ada anggota pemilih tunggal yang memiliki informasi tentang keputusan yang lebih sedikit daripada yang lain.

d) Desentralisasi: VIGOR DAC akan berusaha untuk didesentralisasi tanpa titik sentral kegagalan dan akan berusaha untuk selamanya, tidak rentan terhadap penutupan oleh aktor tunggal atau beberapa individu atau individu, dan mandiri secara finansial untuk seluruh durasinya.

e) Mutabilitas Didelegasikan: Protokol VIGOR hanya dapat ditingkatkan melalui konsensus Multisig dari para anggotanya.

1.3. Sejauh diizinkan, semua ketentuan Konstitusi ini akan ditafsirkan secara konsisten dengan prinsip-prinsip inti yang dinyatakan dalam artikel ini.

## 2. KEANGGOTAAN

2.1. Keanggotaan adalah hasil dari penandatanganan digital Konstitusi Vigor DAC sebagai terdaftar secara permanen di blockchain. Siapa pun yang memiliki kapasitas hukum untuk melakukannya dipersilakan untuk bergabung sebagai Anggota.

2.2. Keanggotaan adalah prasyarat untuk menggunakan protokol VIGOR dengan cara apa pun.

2.3. Anggota di masa depan dan dalam keadaan tertentu dapat diberikan hak suara pada pemilihan Kustodian dan topik lainnya.

2.4. Keanggotaan adalah prasyarat wajib untuk memenuhi syarat sebagai Kandidat VIGOR DAC.

2.5. Anggota dapat mengundurkan diri kapan saja dari VIGOR DAC dan tindakan ini akan direkam secara permanen di blockchain. Anggota yang kehilangan keanggotaan dengan cara ini tidak akan dapat bergabung lagi sebelum 6 bulan kalender.

## 3. KANDIDAT

3.1. Kandidat adalah Anggota yang bermaksud memenuhi syarat untuk menjadi Kustodian.

3.2. Agar Anggota mendaftar sebagai Kandidat, mereka harus menunjukkan partisipasi, komitmen kepada VIGOR DAC dan proyek-proyeknya dan meneruskan permintaan kepada Dewan Kustodian. Jika disetujui, mereka akan menerima token VOTESTAKE yang tidak dapat dijual, didaftarkan, ditebus, atau ditransfer dengan cara apa pun kecuali dengan mengembalikannya ke DAC sebagai saham yang tidak dapat dikembalikan, sebagai catatan berantai yang mengindikasikan dan memungkinkan mereka memenuhi syarat. untuk menerima suara dari Kustodian.

3.3. Selama Kandidat tidak dipilih ke dalam Dewan Kustodian dan menjadi Kustodian, token VOTESTAKE yang dipertaruhkan mereka tidak akan memungkinkan mereka untuk memberikan suara yang efektif, tetapi hanya menyatakan preferensi yang tidak mengikat.

## 4. KUSTODIAN

4.1. Kustodian dipilih melalui pemungutan suara dari Kustodian lain, di antara kumpulan Kandidat terdaftar yang ada, dengan dua puluh satu (21) Kandidat yang menerima jumlah suara tertinggi yang ditunjuk untuk melayani sebagai Kustodian (jumlah Kustodian dapat dikonfigurasi oleh proposal DAC). Kustodian baru menerima token VOTESTAKE yang tidak dapat dipindahtangankan sementara Kustodian yang digulingkan dapat dicabut (VOTESTAKE diambil kembali oleh DAC). Dua puluh satu Kandidat yang terpilih adalah Dewan Kustodian. Jika Kandidat atau Kustodian yang ada menerima jumlah suara yang sama, prioritas akan alfabet berdasarkan nama akun.

4.2. Setiap Kustodian berhak untuk memberikan maksimal dua puluh satu (21) suara untuk memilih seorang Kustodian (jumlah maksimum suara dapat dikonfigurasi oleh proposal DAC), tetapi mereka hanya dapat memberikan 1 suara tunggal untuk setiap Kandidat tunggal atau Kustodian yang ada, termasuk untuk diri mereka sendiri.

4.3. Setiap Kustodian berhak mengeluarkan Proposal tentang topik apa pun kepada Dewan Kustodian, termasuk melibatkan pengeluaran aset fiat atau crypto dalam bentuk apa pun, selama Proposal tersebut kompatibel dengan Prinsip-Prinsip Inti VIGOR DAC dan secara keseluruhan dengan Konstitusi ini.

4.4 Setiap Kustodian berhak memberikan satu (1) suara untuk menyetujui Proposal. Jika Proposal mencapai jumlah suara yang diperlukan untuk tingkat izin yang dipilih oleh pengusul (misalnya, level tinggi saat ini membutuhkan 11 dari 21 suara), itu disetujui dan dapat ditindaklanjuti.

4.5. Kustodian dapat mengusulkan untuk mengubah konfigurasi di DAC termasuk tetapi tidak terbatas pada jumlah kustodian dan ambang batas untuk tiga tingkat izin (tinggi, sedang, rendah). Tingkat izin tinggi adalah untuk Proposal Luar Biasa dengan kuorum yang lebih tinggi untuk persetujuan untuk memutuskan hal-hal luar biasa yang sangat sensitif yang akan secara khusus tercantum dalam Konstitusi ini.

4.6. Voting untuk memilih seorang Kustodian dieksekusi sekali per periode. Panjang periode dapat dikonfigurasi oleh suara DAC. Setiap Kustodian karena itu akan memegang jabatan untuk jangka waktu satu (1) periode, dimulai setiap kali ada orang yang berhasil memanggil tindakan periode baru pada kontrak DAC yang dapat dihubungi pada frekuensi paling banyak satu kali per periode tanpa izin yang diperlukan.

4.7. Setiap Pembayaran atau kompensasi lain untuk Kustodian diizinkan dan akan ditetapkan hanya dengan suara Dewan Kustodian. Pembayaran harus proporsional dengan pekerjaan aktual dan tanggung jawab masing-masing Kustodian dan harus konsisten dengan arus kas yang ada yang berasal dari Sumber Penghasilan VIGOR DAC sebagaimana dinyatakan dalam pasal 7 di bawah ini.

4.8. Kustodian yang meninggal, mengundurkan diri, atau dihapus segera menghentikan kantor mereka, tetapi setiap suara yang sudah diberikan oleh Kustodian itu dalam periode itu, sebelum penghentian tersebut, masih akan valid.

4.9. Kekosongan dalam Dewan Kustodian harus segera diisi, untuk sisa masa jabatan, dengan penunjukan otomatis Kandidat yang memegang jumlah suara terbanyak pada daftar kandidat pemilih, tetapi saat ini tidak melayani sebagai Kustodian, pada saat penciptaan lowongan.

4.10. Tanpa mengurangi penunjukan pengganti Kustodian ex 4.9 di atas, Kustodian yang berkelanjutan dapat bertindak terlepas dari kekosongan dalam Dewan Kustodian, kecuali bahwa di mana jumlah Kustodian dikurangi di bawah angka yang ditetapkan oleh atau sesuai dengan Konstitusi ini sebagai kuorum yang diperlukan untuk Dewan Kustodian, dan tidak ada Kustodian Pengganti yang tersedia, Kustodian atau Kustodian yang berkelanjutan dapat menunjuk langsung Kustodian baru untuk mengisi lowongan apa pun yang telah muncul oleh Resolusi.

## 5. TANGGUNG JAWAB KUSTODIAN

5.1. Operasi dan urusan VIGOR DAC, termasuk tetapi tidak terbatas pada tata kelola dan administrasi aset dan liabilitasnya, akan menjadi hak, ditentukan dan dikelola oleh dan melalui Dewan Kustodian, sebagaimana dibentuk dari waktu ke waktu, yang akan memegang dan melaksanakan semua wewenang tersebut sesuai dengan dan sesuai dengan Konstitusi ini dan Prinsip Inti, dan untuk tujuan tersebut di atas, Dewan Kustodian dapat melakukan semua tindakan, masalah dan hal-hal, dan melaksanakan semua kontrak, instrumen, perbuatan atau dokumen lain, apa pun dan di mana pun, untuk dan atas nama VIGOR DAC.

5.2. Dewan Kustodian dapat, dengan atau tanpa alasan, menghapus Anggota Kustodian, Kandidat atau Anggota lainnya dan mencabut token VOTESTAKE melalui penggunaan Proposal Luar Biasa dan kuorum terkait. Anggota, Kandidat atau Kustodian dihapus dengan cara ini akan secara permanen dilarang bergabung lagi dengan VIGOR DAC.

## 6. REPRESENTASI HUKUM

6.1. VIGOR DAC bukan badan hukum di luar platformnya, dan karena itu dapat membentuk entitas eksternal dengan perwakilan hukum jika itu dalam kepentingan terbaiknya dan sesuai dengan Konstitusi ini.

6.2. Entitas yang dibuat harus bersifat nirlaba dan harus setuju untuk menegakkan Prinsip Inti Konstitusi ini.

6.3. Entitas dapat dibentuk di Negara/Yurisdiksi mana pun asalkan legal, memberikan pertanggungjawaban dan pemisahan patrimonial dan menjunjung tinggi Konstitusi ini.

6.4. Dewan Kustodian akan memilih hingga 5 Kustodian untuk bertindak sebagai anggota, direktur, atau wali amanat dengan kekuasaan pengambilan keputusan atas badan hukum. Kelima Kustodian ini akan dirotasi berdasarkan jadwal yang diputuskan oleh Dewan Kustodian dan dapat dihapus dengan atau tanpa alasan dari kantor ini - tanpa mengurangi posisi mereka dalam VIGOR DAC - dengan memilih Dewan Kustodian. Kelima Kustodian harus secara rutin melaporkan kepada Dewan Kustodian tentang aktivitas apa pun dari badan hukum.

6.5. Anggaran Dasar entitas harus dirancang sedemikian rupa untuk menjamin kendali VIGOR DAC atasnya; entitas tidak akan memiliki atau memiliki kendali atas VIGOR DAC itu sendiri.

6.6. Jika disyaratkan oleh undang-undang dan peraturan yurisdiksi/Negara tempat badan hukum tersebut berbasis, Direktur independen pihak ketiga dan/atau Wali Amanat dapat dinominasikan sesuai kebutuhan tetapi kekuasaan pengambilan keputusan pamungkas akan selalu berada dalam 5 Kustodian yang ditunjuk oleh Dewan Kustodian.

6.7. Setiap Kustodian yang bersedia menjadi salah satu dari 5 anggota badan hukum akan dikenakan pemeriksaan KYC dan AML yang ketat dan dapat ditolak atau ditolak oleh Direktur dan/atau Wali Amanat pihak ketiga entitas jika ada unsur negatif akan muncul dari pemeriksaan latar belakang ini.

## 7. SUMBER PENDAPATAN

7.1. VIGOR DAC secara langsung memiliki sebagian dari total token yang diterbitkan token VIG dari Protokol VIGOR dan dapat menggunakan token ini sebagaimana diputuskan oleh Dewan Kustodian untuk meneruskan, mempromosikan, mengelola, mengembangkan, dan melaksanakan kegiatan atau prinsip apa pun yang dinyatakan dalam Konstitusi ini dan Inti Prinsipnya.

7.2. Adalah mungkin bagi Dewan Kustodian untuk melembagakan persentase komisi pada beberapa kegiatan Protokol VIGOR untuk memastikan pendanaan dan pemeliharaan VIGOR DAC yang tepat, Anggota-anggotanya, Kandidat, Kustodian, dan Protokol VIGOR itu sendiri. Pengeluaran aset yang diperoleh dapat digunakan dengan cara yang sama seperti dalam pasal 7.1. atas.

7.3. Penghasilan atau pengeluaran token apa pun yang dihasilkan atau dimiliki oleh VIGOR DAC di luar pengelolaan Protokol VIGOR yang melebihi biaya operasional (termasuk Pembayaran Kustodian) tidak akan pernah dapat didistribusikan kepada Anggota, Kandidat atau Kustodian dan karenanya ditafsirkan sebagai keuntungan atau dividen dari setiap baik, dan harus disisihkan untuk pengeluaran di masa depan.

## 8. AMANDEMEN

8.1. Setiap amandemen Konstitusi ini harus diajukan oleh Kustodian sebagai Proposal Luar Biasa dan dipilih oleh Dewan Kustodian dengan kuorum yang lebih tinggi yang diperlukan. Konstitusi yang diubah harus diunggah ke blockchain dan semua Anggota, Kandidat dan Kustodian yang ada harus menandatanganinya kembali secara digital atau secara otomatis dihapus dari VIGOR DAC dalam waktu 7 hari solar sejak pengunggahan.

## 9. DISOLUSI VOLUNTARIS

9.1. Pembebasan Sukarela hanya dapat diusulkan dan dipilih melalui Proposal Luar Biasa dan dengan kuorum suara terkait.

9.2. Dalam kasus Pembebasan Sukarela aset terutang yang dimiliki oleh VIGOR DAC secara langsung atau melalui kendali Protokol VIGOR tidak akan pernah dapat didistribusikan kepada salah satu Anggotanya, Kandidat atau Kustodian tetapi sebaliknya harus disumbangkan dan dipindahkan ke proyek Desentralized Finance (DeFi) nirlaba lain dari Dewan Kustodian yang memilih atau, jika tidak ada asosiasi nirlaba atau organisasi amal lainnya yang dipilih oleh Dewan Kustodian.

## 9. HUKUM DAN YURISDIKSI YANG BERLAKU

9.1. Konstitusi ini dan setiap perselisihan atau klaim (termasuk perselisihan atau klaim non-kontrak) yang timbul dari atau sehubungan dengan itu akan diatur oleh atau ditafsirkan sesuai dengan hukum Inggris dan Wales. Pengadilan Inggris dan Wales akan memiliki yurisdiksi eksklusif untuk menyelesaikan perselisihan atau klaim (termasuk sengketa atau klaim non-kontrak) yang timbul dari atau sehubungan dengan Konstitusi ini.

